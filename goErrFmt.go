package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
)

func main() {
	err := filepath.Walk("./", FormatFile)
	if err != nil {
		fmt.Println(err)
	}
}

func FormatFile(fp string, fi os.FileInfo, err error) error {
	if err != nil {return err}
	if fi.IsDir() {
		return nil
	}
	matched, err := filepath.Match("*.go", fi.Name());	if err != nil {return err}
	if matched {
		content, err := ioutil.ReadFile(fp);	if err != nil {return err}
		fmt.Println(fp)
		re := regexp.MustCompile("\n\\s*(?P<intro>if err != nil \\{)\n\\s*(?P<return>.*)\n\\s*(?P<close>\\})")
		newString := re.ReplaceAllString(string(content), ";	$intro$return$close")
		ioutil.WriteFile(fp, []byte(newString), 0644)
	}
	return nil
}
