# goErrFmt
A tool to deal with formatting error handling in Go code.

Simple return statements inside a `if err != nil` will be moved inline so to not disrupt code flow
